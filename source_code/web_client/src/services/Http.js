import axios from 'axios'
const { VITE_API_URL, VITE_TIMEOUT } = import.meta.env

const requestConfig = {
  baseURL: VITE_API_URL,
  timeout: +VITE_TIMEOUT,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
  }
}

const $http = axios.create(requestConfig)

// $http.defaults.baseURL = VITE_API_URL;
// $http.defaults.timeout = VITE_TIMEOUT;

// 添加请求拦截器
$http.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    config.headers['Authorization'] = localStorage.getItem('token')
    // config.headers['token'] = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImRhdGEiOnsidXNlcm5hbWUiOiJXRCIsInBhc3N3b3JkIjoid2QxIn19fQ.iIbPBzQdnHDP3T7v95aT2y7dWjrukL9lL081nfzSVKI";
    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
$http.interceptors.response.use(
  function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    const { error } = response.data
    if (error) {
      response.message = decodeURIComponent(error)
    }
    return response
  },
  function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么

    return Promise.reject(error)
  }
)

export function urlFor(path) {
  return VITE_API_URL + path
}

export default $http
