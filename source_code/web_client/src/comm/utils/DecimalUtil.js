import { Decimal } from 'decimal.js'

export const ZERO = () => new Decimal(0)
export const ONE = () => new Decimal(1)
export const TEN = () => new Decimal(10)

export { Decimal }
