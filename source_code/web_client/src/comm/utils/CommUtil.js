import { defineAsyncComponent, hydrateOnVisible } from 'vue'
import equal from 'fast-deep-equal'

/* *
深度比较两个对象是否相等
*/
export { equal }

/* *定义异步组件 */
export const AsyncComponent = function (path, config = {}) {
  return defineAsyncComponent({
    loader: () => import(path),
    // 展示加载组件前的延迟时间，默认为 200ms
    delay: 200,
    // 加载失败后展示的组件
    errorComponent: ErrorComponent,
    // 如果提供了一个 timeout 时间限制，并超时了
    // 也会显示这里配置的报错组件，默认值是：Infinity
    timeout: 3000,
    hydrate: hydrateOnVisible()
  })
}

export const importComponent = function (path) {
  return defineAsyncComponent(() => import(path))
}
