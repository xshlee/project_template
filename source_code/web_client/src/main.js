import '@/assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from '@/App.vue'
import router from '@/router'
import { mountUI } from '@/components'

const app = createApp(App)
app.use(createPinia())
app.use(router)

mountUI(app)

app.mount('#app')
