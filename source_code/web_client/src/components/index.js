/* eslint-disable vue/no-reserved-component-names */
/* eslint-disable vue/multi-word-component-names */

import 'primeicons/primeicons.css'
import PrimeVue from 'primevue/config'
import ToastService from 'primevue/toastservice'
import DialogService from 'primevue/dialogservice'

//import useXXXX
import { useDialog } from 'primevue/usedialog'
import { useToast } from 'primevue/usetoast'
import { Form, Field, ErrorMessage, useForm, useField } from 'vee-validate'
import uiconfig from '@/configs/ui'

const components = [Form, Field, ErrorMessage]

export function mountUI(app) {
  app.use(PrimeVue, uiconfig)
  app.use(ToastService)
  app.use(DialogService)

  components.forEach((c) => app.component(c.name, c))
}

export { useDialog, useToast, useForm, useField }
// export const DynamicForm = defineAsyncComponent(() => import('./DynamicForm.vue'))
