import { storeToRefs } from 'pinia'
import useAppStore from './app'

/**
 *
 * @param {*} useStore
 * @param {*} type {'array' | 'object'}
 * @returns
 */
export function insStoreRefs(useStore, type = 'array') {
  const store = useStore()
  return type == 'object' ? { store, refs: storeToRefs(store) } : [store, storeToRefs(store)]
}

/**
 *
 * @param {*} useStores
 * @param {*} type {'array' | 'object'}
 * @returns
 */
export function insStoreRefsArray(useStores = [], type = 'array') {
  return useStores.map((useStore) => insStoreRefs(useStore, type))
}

export { useAppStore, storeToRefs }
