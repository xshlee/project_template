import Aura from '@primevue/themes/aura'
export default {
  theme: {
    ripple: false, //涟漪效果
    inputVariant: 'filled',
    unstyled: true,
    preset: Aura,
    options: {
      prefix: '',
      darkModeSelector: '', //system,light,dark
      cssLayer: {
        name: 'primevue',
        order: 'tailwind-base, primevue, tailwind-utilities'
      }
    }
  },
  ptOptions: {
    mergeSections: true,
    mergeProps: true
  }
}
